package raphaelrocha.desafioandroid.interfaces;

import android.view.View;

public interface RecyclerViewOnClickListener {
    void onClickListener(View view, int position);
    void onLongPressClickListener(View view, int position);
}
