package raphaelrocha.desafioandroid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import raphaelrocha.desafioandroid.connection.services.VolleyConnectionQueue;

public class BaseActivity extends AppCompatActivity {

    /*
    * Todas as activities dessa app estendem essa Activity de base.
    * */

//    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inicia a fila para controle de requisições HTTP
        VolleyConnectionQueue.getINSTANCE().startQueue(this);
    }

    public void showToast(String msg, boolean isShowLong) {

        Toast toast;
        if(isShowLong){
            toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        }else {
            toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        }
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }
}
