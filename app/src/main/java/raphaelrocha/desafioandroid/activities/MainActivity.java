package raphaelrocha.desafioandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import raphaelrocha.desafioandroid.R;
import raphaelrocha.desafioandroid.adapters.RepoAdapter;
import raphaelrocha.desafioandroid.app.Config;
import raphaelrocha.desafioandroid.connection.ErrorMessage;
import raphaelrocha.desafioandroid.connection.Routes;
import raphaelrocha.desafioandroid.connection.RoutesEnum;
import raphaelrocha.desafioandroid.connection.VolleyConnection;
import raphaelrocha.desafioandroid.connection.interfaces.CustomVolleyCallbackInterface;
import raphaelrocha.desafioandroid.interfaces.RecyclerViewOnClickListener;
import raphaelrocha.desafioandroid.models.Repo;
import raphaelrocha.desafioandroid.models.User;
import raphaelrocha.desafioandroid.utils.PrefManager;

public class MainActivity extends BaseActivity implements View.OnClickListener, CustomVolleyCallbackInterface {

//    private final String TAG = this.getClass().getSimpleName();
    private final String LIST_REPOSITORIES = "list_repos";
    private final String COUNT_PAGE_TO_LOAD = "count_page_to_load";
    private final String IS_LOGIN_OPEN_STATUS = "is_login_open_status";
    private final String IS_REFRESH_STATUS = "is_refresh_status";
    private final String IS_ERROR_STATUS = "is_error_status";
    private boolean mIsRefreshSw;
    private boolean mIsError;
    private boolean mIsLogginOpen;
    private Toolbar mToobar;
    private Bundle mSavedInstanceState;
    private Drawer mNavigationDrawer;
    private ArrayList<Repo> mListRepos = new ArrayList<>();
    private RepoAdapter mAdapter;
    private SwipeRefreshLayout mSwrf;
    private LinearLayout mLlMsgError;
    private LinearLayout mLlMsgNoItems;
    private VolleyConnection mConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSavedInstanceState = savedInstanceState;

        mConnection = new VolleyConnection(this,this);

        mToobar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToobar);

        mLlMsgError = (LinearLayout) findViewById(R.id.repo_ll_offline_alert);
        mLlMsgNoItems = (LinearLayout) findViewById(R.id.repo_ll_no_items);
        mSwrf = (SwipeRefreshLayout) findViewById(R.id.refresh_repo);
        TextView mTvInfo = (TextView) findViewById(R.id.tv_info);

        if(mSwrf!=null){
            mSwrf.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    mIsRefreshSw = true;
                    mAdapter.resetPageToLoad();
                    getListRepoFromServer(Config.DEFAULT_PAGE_REPO_LIST);
                }
            });
        }

        if(mLlMsgError!=null){
            mLlMsgError.setOnClickListener(this);
        }

        if(mLlMsgNoItems!=null){
            mLlMsgNoItems.setOnClickListener(this);
        }

        if(mTvInfo !=null){
            mTvInfo.setText(getResources().getString(R.string.no_repos_msg));
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_repo);
        mAdapter = new RepoAdapter(mListRepos,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.setRecyclerViewOnClickListener(new RecyclerViewOnClickListener() {
            @Override
            public void onClickListener(View view, int position) {
                Repo repo = mAdapter.getItem(position);

                Intent intent = new Intent(MainActivity.this, RepoDetailActivity.class);
                intent.putExtra("repo",repo);
                startActivity(intent);
            }

            @Override
            public void onLongPressClickListener(View view, int position) {

            }
        });
        recyclerView.setAdapter(mAdapter);

        updateToobar(false,getResources().getString(R.string.app_name),getResources().getString(R.string.app_subtitle));

        setDrawer();
    }

    public void getListRepoFromServer(int page){
        /*
        Lista todos os repositórios da página selecionada
         */
        showProgress(true);
        String url = Routes.REPO_LIST.replace(":page", String.valueOf(page));
        mConnection.callServerApiByJsonObjectRequest(url, Request.Method.GET,false,null, RoutesEnum.REPOS_LIST);
    }

    public void forceShowProgress(){
        if(!mIsError){
            mSwrf.setRefreshing(true);
        }
    }

    private void setDrawer(){
//        Log.w(TAG,"load material drawer");
        DrawerImageLoader.init(new DrawerImageLoader.IDrawerImageLoader() {
            @Override
            public void set(final ImageView imageView, final Uri uri, Drawable placeholder) {
                //Picasso.with(imageView.getContext()).load(uri).placeholder(placeholder).error(R.drawable.avatar_default).into(imageView);
                Picasso.with(imageView.getContext())
                    .load(uri)
                    .placeholder(ContextCompat.getDrawable(imageView.getContext(),R.drawable.avatar_default))
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(imageView.getContext())
                                .load(uri)
                                .error(ContextCompat.getDrawable(imageView.getContext(),R.drawable.avatar_default))
                                .placeholder(ContextCompat.getDrawable(imageView.getContext(),R.drawable.avatar_default))
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
//                                        Log.v("Picasso","Could not fetch image");
                                    }
                                });
                        }
                    });
            }

            @Override
            public void cancel(ImageView imageView) {
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx) {
                return null;
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                return null;
            }
        });

        final User user = PrefManager.getUser(this);

        IProfile mProfile;
        if(user!=null){
            mProfile = new ProfileDrawerItem().withIcon(user.getAvatarUrl()).withName(user.getName()).withEmail(user.getEmail());
        }else{
            Drawable img = ContextCompat.getDrawable(this,R.drawable.avatar_default);
            String name = getResources().getString(R.string.app_anonymous_name);
            String email = getResources().getString(R.string.app_anonymous_email);
            mProfile = new ProfileDrawerItem().withIcon(img).withName(name).withEmail(email);
        }

        AccountHeader headerNavigationDrawer = new AccountHeaderBuilder()
            .withActivity(this)
            .withSavedInstance(mSavedInstanceState)
            .withProfileImagesClickable(true)
            .withSelectionListEnabledForSingleProfile(false)
            .withTextColor(ContextCompat.getColor(this, R.color.md_white_1000))
            .withHeaderBackground(R.color.accent)
            .addProfiles(mProfile)
            .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                @Override
                public boolean onProfileChanged(View view, IProfile iProfile, boolean b) {
                    if(user==null){
                        openLoginPage();
                    }
                    return false;
                }
            })
            .build();

        //Implementacao do drawer
        mNavigationDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToobar)
                .withDrawerGravity(Gravity.START)
                .withSavedInstance(mSavedInstanceState)
                .withActionBarDrawerToggleAnimated(true)
                .withActionBarDrawerToggle(true)
                .withSliderBackgroundColor(ContextCompat.getColor(this,R.color.primary))
                .withSelectedItem(-1)
                .withAccountHeader(headerNavigationDrawer)
                .withTranslucentStatusBar(true)
                .build();

        if(PrefManager.isLogged(this)){
            PrimaryDrawerItem primaryDrawerItem2 = new PrimaryDrawerItem();
            primaryDrawerItem2.withName(getResources().getString(R.string.logout));
            primaryDrawerItem2.withIdentifier(1);
            primaryDrawerItem2.withIcon(ContextCompat.getDrawable(this, R.drawable.ic_logout));
            primaryDrawerItem2.withSelectedTextColorRes(R.color.accent);
            primaryDrawerItem2.withTextColorRes(R.color.md_white_1000);
            primaryDrawerItem2.withSelectedColor(ContextCompat.getColor(this,R.color.primary_dark));
            mNavigationDrawer.addItem(primaryDrawerItem2);
            mNavigationDrawer.addItem(new DividerDrawerItem());
        }else{
            PrimaryDrawerItem primaryDrawerItem2 = new PrimaryDrawerItem();
            primaryDrawerItem2.withName(getResources().getString(R.string.login));
            primaryDrawerItem2.withIdentifier(2);
            primaryDrawerItem2.withIcon(ContextCompat.getDrawable(this, R.drawable.ic_login));
            primaryDrawerItem2.withSelectedTextColorRes(R.color.accent);
            primaryDrawerItem2.withTextColorRes(R.color.md_white_1000);
            primaryDrawerItem2.withSelectedColor(ContextCompat.getColor(this,R.color.primary_dark));
            mNavigationDrawer.addItem(primaryDrawerItem2);
            mNavigationDrawer.addItem(new DividerDrawerItem());
        }

        mNavigationDrawer.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                switch (drawerItem.getIdentifier()){
                    case 1:
                        logout();
                        break;

                    case 2:
                        openLoginPage();
                        break;
                }

                return false;
            }
        });
    }

    private void openLoginPage(){
        mIsLogginOpen = true;
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    private void logout(){
        PrefManager.clearUser(this);
        showToast(getResources().getString(R.string.logout_message),false);
        setDrawer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mIsLogginOpen){
            setDrawer();
            mIsLogginOpen=false;
        }
        if(mAdapter.isTimeToLoad()){
            getListRepoFromServer(mAdapter.getPageToLoadCount());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void updateToobar(boolean backHome, String title, String subTitle){

        TextView toolbarTittleText = (TextView) findViewById(R.id.toolbar_title);
        TextView toolbarSubTittleText = (TextView) findViewById(R.id.toolbar_subtitle);
        if(backHome){
            ImageView backButton = (ImageView) findViewById(R.id.back_button);
            if(backButton!=null){
                backButton.setOnClickListener(this);
            }
        }

        if(getSupportActionBar()!=null){
            toolbarTittleText.setText(title);
            toolbarSubTittleText.setText(subTitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if(mNavigationDrawer.isDrawerOpen()){
            mNavigationDrawer.closeDrawer();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mIsLogginOpen = savedInstanceState.getBoolean(IS_LOGIN_OPEN_STATUS);
            mIsRefreshSw = savedInstanceState.getBoolean(IS_REFRESH_STATUS);
            mIsError = savedInstanceState.getBoolean(IS_ERROR_STATUS);
            mListRepos = savedInstanceState.getParcelableArrayList(LIST_REPOSITORIES);
            mAdapter.setPageToLoadCount(savedInstanceState.getInt(COUNT_PAGE_TO_LOAD));
            mAdapter.setList(mListRepos);
        }else{
            getListRepoFromServer(Config.DEFAULT_PAGE_REPO_LIST);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(LIST_REPOSITORIES,mListRepos);
        outState.putInt(COUNT_PAGE_TO_LOAD,mAdapter.getPageToLoadCount());
        outState.putBoolean(IS_LOGIN_OPEN_STATUS,mIsLogginOpen);
        outState.putBoolean(IS_REFRESH_STATUS,mIsRefreshSw);
        outState.putBoolean(IS_ERROR_STATUS,mIsError);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_button:
                onBackPressed();
                break;

            case R.id.repo_ll_offline_alert:
                if(mLlMsgError.getVisibility()==View.VISIBLE){
                    mLlMsgError.setVisibility(View.GONE);
                }
                getListRepoFromServer(Config.DEFAULT_PAGE_REPO_LIST);
                break;

            case R.id.repo_ll_no_items:
                if(mLlMsgNoItems.getVisibility()==View.VISIBLE){
                    mLlMsgNoItems.setVisibility(View.GONE);
                }
                getListRepoFromServer(Config.DEFAULT_PAGE_REPO_LIST);
                break;
        }
    }

    private void showProgress(final boolean show) {
        if(mListRepos.size()==0){
            mSwrf.setRefreshing(show);
        }else{
            if(mSwrf.isRefreshing()){
                mSwrf.setRefreshing(false);
            }
        }
    }

    public void setList(JSONObject jo){
        //limpa a lista caso o a tela tenha sido atualizada pelo swiperefresh
        if(mIsRefreshSw){
            mIsRefreshSw = false;
            mListRepos.clear();
            showProgress(false);
        }
        try {
            JSONArray ja = jo.getJSONArray("items");
            Gson gson = new Gson();

            for(int i=0;i<ja.length();i++){
                Repo repo = gson.fromJson(ja.getJSONObject(i).toString(), Repo.class);
                mListRepos.add(repo);
            }

            if(mListRepos.size()>0){
                mAdapter.setList(mListRepos);
                if(mLlMsgNoItems.getVisibility()==View.VISIBLE){
                    mLlMsgNoItems.setVisibility(View.GONE);
                }

            }else{
                if(mLlMsgNoItems.getVisibility()==View.GONE){
                    mLlMsgNoItems.setVisibility(View.VISIBLE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*Metodos de callback usado pela classe de conexão*/
    @Override
    public void deliveryResponse(JSONArray response, RoutesEnum routeEnum) throws Exception {

    }

    @Override
    public void deliveryResponse(JSONObject response, RoutesEnum routeEnum) throws Exception {
        mIsError = false;
        if(mLlMsgError.getVisibility()==View.VISIBLE){
            mLlMsgError.setVisibility(View.GONE);
        }
        switch (routeEnum){
            case REPOS_LIST:
                showProgress(false);
                setList(response);
                break;
        }
    }

    @Override
    public void deliveryError(ErrorMessage errorMessage, RoutesEnum routeEnum) throws Exception {
//        Log.e(TAG,errorMessage.toString());
        showToast(errorMessage.getMessage(),false);
        if(mListRepos.size()==0){
            if(mLlMsgError.getVisibility()==View.GONE){
                mLlMsgError.setVisibility(View.VISIBLE);
            }
        }
        mIsError = true;
        showProgress(false);
    }

    @Override
    public void onStop(){
        super.onStop();
        mConnection.cancelRequest();
    }
}
