package raphaelrocha.desafioandroid.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import raphaelrocha.desafioandroid.R;
import raphaelrocha.desafioandroid.adapters.PullAdapter;
import raphaelrocha.desafioandroid.connection.ErrorMessage;
import raphaelrocha.desafioandroid.connection.Routes;
import raphaelrocha.desafioandroid.connection.RoutesEnum;
import raphaelrocha.desafioandroid.connection.VolleyConnection;
import raphaelrocha.desafioandroid.connection.interfaces.CustomVolleyCallbackInterface;
import raphaelrocha.desafioandroid.interfaces.RecyclerViewOnClickListener;
import raphaelrocha.desafioandroid.models.Pull;
import raphaelrocha.desafioandroid.models.Repo;

public class RepoDetailActivity extends BaseActivity implements CustomVolleyCallbackInterface, View.OnClickListener {

//    private final String TAG = this.getClass().getSimpleName();
    private final String LIST_PULLS = "list_pulls";
    private final String REPOSITORIE = "repositorie";
    private VolleyConnection mConnection;
    private Repo mRepo;
    private PullAdapter mAdapter;
    private ArrayList<Pull> mListPulls = new ArrayList<>();
    private SwipeRefreshLayout mSwrf;
    private LinearLayout mLlMsgError;
    private LinearLayout mLlMsgNoItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_detail);

        mConnection = new VolleyConnection(this,this);

        mRepo = getIntent().getParcelableExtra("repo");

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mRepo.getName());
            getSupportActionBar().setSubtitle(mRepo.getOwner().getLogin());
        }

        mSwrf = (SwipeRefreshLayout) findViewById(R.id.refresh_pulls);
        mLlMsgError = (LinearLayout) findViewById(R.id.pull_ll_offline_alert);
        mLlMsgNoItems = (LinearLayout) findViewById(R.id.pull_ll_no_items);
        TextView mTvInfo = (TextView) findViewById(R.id.tv_info);

        if(mSwrf!=null){
            mSwrf.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    callServer();
                }
            });
        }

        if(mLlMsgError!=null){
            mLlMsgError.setOnClickListener(this);
        }

        if(mTvInfo !=null){
            mTvInfo.setText(getResources().getString(R.string.no_pulls_msg));
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_pull);
        mAdapter = new PullAdapter(mListPulls,this);
        RecyclerView.LayoutManager mLayoutManagerCases = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManagerCases);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.setRecyclerViewOnClickListener(new RecyclerViewOnClickListener() {
            @Override
            public void onClickListener(View view, int position) {

            }

            @Override
            public void onLongPressClickListener(View view, int position) {

            }
        });
        recyclerView.setAdapter(mAdapter);

        //faz o setupo do toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateToobar(true, mRepo.getName(), mRepo.getOwner().getLogin());
    }

    private void updateToobar(boolean backHome, String title, String subTitle){
//        Log.w(TAG,"updateToobar");
        TextView toolbarTittleText = (TextView) findViewById(R.id.toolbar_title);
        TextView toolbarSubTittleText = (TextView) findViewById(R.id.toolbar_subtitle);
        if(backHome){
            ImageView backButton = (ImageView) findViewById(R.id.back_button);
            if(backButton!=null){
                backButton.setOnClickListener(this);
                backButton.setVisibility(View.VISIBLE);
            }
        }
        if(getSupportActionBar()!=null){
            toolbarTittleText.setText(title);
            toolbarSubTittleText.setText(subTitle);
        }
    }

    private void showProgress(final boolean show) {
        mSwrf.setRefreshing(show);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        callServer();
    }

    private void setList(JSONArray ja){
        try {
            mListPulls.clear();
            Gson gson = new Gson();

            for(int i=0;i<ja.length();i++){
                Pull pull = gson.fromJson(ja.getJSONObject(i).toString(), Pull.class);
                mListPulls.add(pull);
            }

            if(mListPulls.size()>0){
                mAdapter.setList(mListPulls);
                if(mLlMsgNoItems.getVisibility()==View.VISIBLE){
                    mLlMsgNoItems.setVisibility(View.GONE);
                }

            }else{
                if(mLlMsgNoItems.getVisibility()==View.GONE){
                    mLlMsgNoItems.setVisibility(View.VISIBLE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callServer(){
        showProgress(true);
        String url = Routes.PULLS_REPO_LIST.replace(":owner_login",mRepo.getOwner().getLogin());
        url = url.replace(":repo_name",mRepo.getName());
        mConnection.callServerApiByJsonArrayRequest(url, Request.Method.GET,false,null,RoutesEnum.PULLS_REPO_LIST);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mListPulls = savedInstanceState.getParcelableArrayList(LIST_PULLS);
            mRepo = savedInstanceState.getParcelable(REPOSITORIE);
            mAdapter.setList(mListPulls);
        }else{
            callServer();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(LIST_PULLS,mListPulls);
        outState.putParcelable(REPOSITORIE,mRepo);
    }

    @Override
    public void deliveryResponse(JSONArray response, RoutesEnum routeEnum) throws Exception {
        if(mLlMsgError.getVisibility()==View.VISIBLE){
            mLlMsgError.setVisibility(View.GONE);
        }
        switch (routeEnum){
            case PULLS_REPO_LIST:
                showProgress(false);
                setList(response);
                break;
        }
    }

    /*Metodos de callback usado pela classe de conexão*/
    @Override
    public void deliveryResponse(JSONObject response, RoutesEnum routeEnum) throws Exception {

    }

    @Override
    public void deliveryError(ErrorMessage errorMessage, RoutesEnum routeEnum) throws Exception {
        showToast(errorMessage.getMessage(),false);
        if(mLlMsgError.getVisibility()==View.GONE){
            mLlMsgError.setVisibility(View.VISIBLE);
        }
        showProgress(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.pull_ll_offline_alert:
                callServer();
                break;

            case R.id.back_button:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onStop(){
        super.onStop();
        mConnection.cancelRequest();
    }
}
