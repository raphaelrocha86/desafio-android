package raphaelrocha.desafioandroid.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONObject;

import raphaelrocha.desafioandroid.R;
import raphaelrocha.desafioandroid.connection.Routes;
import raphaelrocha.desafioandroid.connection.RoutesEnum;
import raphaelrocha.desafioandroid.connection.VolleyConnection;
import raphaelrocha.desafioandroid.connection.interfaces.CustomVolleyCallbackInterface;
import raphaelrocha.desafioandroid.connection.ErrorMessage;
import raphaelrocha.desafioandroid.utils.PrefManager;

public class LoginActivity extends BaseActivity implements CustomVolleyCallbackInterface, OnClickListener {

    // UI references.
//    private final String TAG = this.getClass().getSimpleName();
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private VolleyConnection mConnection;
    private boolean isShowPasswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        final ImageButton btnShowPass = (ImageButton) findViewById(R.id.btn_show_pass);

        btnShowPass.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isShowPasswd){
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    btnShowPass.setImageResource(R.drawable.ic_eye_outline);
                    isShowPasswd=false;
                }else{
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT);

                    btnShowPass.setImageResource(R.drawable.ic_eye_outline_off);
                    isShowPasswd=true;
                }
                mPasswordView.setSelection(mPasswordView.getText().length());
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        //Toolbar personalizada
        Toolbar toobar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toobar);
        TextView toolbarTittleText = (TextView) findViewById(R.id.toolbar_title);
        TextView toolbarSubTittleText = (TextView) findViewById(R.id.toolbar_subtitle);
        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        LinearLayout logoImage = (LinearLayout) findViewById(R.id.ll_logo_image);
        if(backButton!=null){
            backButton.setOnClickListener(this);
            if(backButton.getVisibility()==View.GONE){
                backButton.setVisibility(View.VISIBLE);
            }
        }

        if(logoImage!=null && logoImage.getVisibility()==View.VISIBLE){
            logoImage.setVisibility(View.GONE);
        }

        if(getSupportActionBar()!=null){
            toolbarTittleText.setText(getResources().getString(R.string.title_activity_login));
            toolbarSubTittleText.setText(getResources().getString(R.string.subtitle_activity_login));
        }

        mConnection = new VolleyConnection(this,this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void attemptLogin() {

        // reset dos erros.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Valida a senha
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Valida o e-mail
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);

            //salva o token basic no shared preferences pra poder usar na conexão
            String credential = email+":"+password;
            String encodeCredentica = Base64.encodeToString(credential.getBytes(), Base64.NO_WRAP);
            String token = "Basic "+encodeCredentica;
            PrefManager.setToken(this,token);
            //faz a chamada para pegar o objeto de usuário conforme o token gerado
            String url = Routes.USER_DATA;
            mConnection.callServerApiByJsonObjectRequest(url, Request.Method.GET,false,null, RoutesEnum.USER_DATA);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            /*Usa a animação nas versões com suporte*/
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            /*Não usa animação nas versões sem suporte*/
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /*Metodos de callback usado pela classe de conexão*/
    @Override
    public void deliveryResponse(JSONArray response, RoutesEnum routeEnum) throws Exception {

    }

    @Override
    public void deliveryResponse(JSONObject response, RoutesEnum routeEnum) throws Exception {
        showToast(getResources().getString(R.string.login_successfully),false);
        PrefManager.saveUser(this,response);
        finish();
    }

    @Override
    public void deliveryError(ErrorMessage error, RoutesEnum routeEnum) throws Exception {
        showProgress(false);
        showToast(error.getMessage(),false);
        PrefManager.clearToken(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_button:
                onBackPressed();
        }
    }

    @Override
    public void onStop(){
        super.onStop();
        mConnection.cancelRequest();
    }
}

