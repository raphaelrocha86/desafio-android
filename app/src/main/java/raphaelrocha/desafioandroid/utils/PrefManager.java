package raphaelrocha.desafioandroid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

import org.json.JSONObject;

import raphaelrocha.desafioandroid.models.User;

public class PrefManager {

//    private static String TAG = PrefManager.class.getSimpleName();
    private static final String PREF_NAME = "desafio_android";
    private static final String KEY_USER_TOKEN = "token_user";
    private static final String USER_DATA = "user_data";

    public static void clearToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        Editor editor = pref.edit();
        editor.putString(KEY_USER_TOKEN, null);
        editor.commit();
    }

    public static void setToken(Context context, String token) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        Editor editor = pref.edit();
        editor.putString(KEY_USER_TOKEN, token);
        editor.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        return pref.getString(KEY_USER_TOKEN, null);
    }

    public static void saveUser(Context context, JSONObject user) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        Editor editor = pref.edit();
        editor.putString(USER_DATA, user.toString());
        editor.commit();
    }

    public static User getUser(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        String stringUser = pref.getString(USER_DATA, null);
        Gson gson = new Gson();
        return gson.fromJson(stringUser,User.class);
    }

    public static void clearUser(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        Editor editor = pref.edit();
        editor.putString(USER_DATA, null);
        editor.commit();
        clearToken(context);
    }

    public static boolean isLogged(Context context){
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        String stringUser = pref.getString(USER_DATA, null);
        Gson gson = new Gson();
        User user =  gson.fromJson(stringUser,User.class);

        if(user!=null){
            return true;
        }else{
            return false;
        }
    }
}
