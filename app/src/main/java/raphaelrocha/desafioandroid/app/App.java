package raphaelrocha.desafioandroid.app;

import android.app.Application;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class App extends Application {

//    private String TAG = this.getClass().getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        enablePicassoCache();
    }
    /*
    * habilita o cache de imagens do picasso
    * */
    private void enablePicassoCache(){
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        //habilita ou desabilita o log
        built.setLoggingEnabled(false);
        Picasso.setSingletonInstance(built);
    }
}
