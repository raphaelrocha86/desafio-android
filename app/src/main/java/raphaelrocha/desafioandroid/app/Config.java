package raphaelrocha.desafioandroid.app;

/*
 * Deve conter todas os valores de configuração do App
 */

public class Config {
    public static final int DEFAULT_PAGE_REPO_LIST = 1;
    public static final String OPEN_REPOSITORY = "intentopenrepository";
}
