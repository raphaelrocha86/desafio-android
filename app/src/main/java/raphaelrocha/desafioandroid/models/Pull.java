package raphaelrocha.desafioandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Pull implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("state")
    private String state;
    @SerializedName("title")
    private String title;
    @SerializedName("number")
    private String number;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user")
    private User user;
    @SerializedName("body")
    private String body;

    public String getBody() {
        return body;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public String getTitle() {
        return title;
    }

    public String getNumber() {
        return number;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public User getUser() {
        return user;
    }

    public Pull() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.state);
        dest.writeString(this.title);
        dest.writeString(this.number);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.body);
    }

    protected Pull(Parcel in) {
        this.id = in.readInt();
        this.state = in.readString();
        this.title = in.readString();
        this.number = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.body = in.readString();
    }

    public static final Creator<Pull> CREATOR = new Creator<Pull>() {
        @Override
        public Pull createFromParcel(Parcel source) {
            return new Pull(source);
        }

        @Override
        public Pull[] newArray(int size) {
            return new Pull[size];
        }
    };
}
