package raphaelrocha.desafioandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Repo implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("owner")
    private User owner;
    @SerializedName("description")
    private String description;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("pushed_at")
    private String pushedAt;
    @SerializedName("updated_at")
    private String updateddAt;
    @SerializedName("stargazers_count")
    private int stars;
    @SerializedName("forks_count")
    private int forks;
    @SerializedName("language")
    private String language;

    @Override
    public String toString() {
        return "Repo{" +
                "name='" + name + '\'' +
                ", owner=" + owner +
                ", description='" + description + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", pushedAt='" + pushedAt + '\'' +
                ", updateddAt='" + updateddAt + '\'' +
                ", stars=" + stars +
                ", forks=" + forks +
                ", language='" + language + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public User getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getPushedAt() {
        return pushedAt;
    }

    public String getUpdateddAt() {
        return updateddAt;
    }

    public int getStars() {
        return stars;
    }

    public int getForks() {
        return forks;
    }

    public String getLanguage() {
        return language;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeParcelable(this.owner, flags);
        dest.writeString(this.description);
        dest.writeString(this.createdAt);
        dest.writeString(this.pushedAt);
        dest.writeString(this.updateddAt);
        dest.writeInt(this.stars);
        dest.writeInt(this.forks);
        dest.writeString(this.language);
    }

    public Repo() {
    }

    protected Repo(Parcel in) {
        this.name = in.readString();
        this.owner = in.readParcelable(User.class.getClassLoader());
        this.description = in.readString();
        this.createdAt = in.readString();
        this.pushedAt = in.readString();
        this.updateddAt = in.readString();
        this.stars = in.readInt();
        this.forks = in.readInt();
        this.language = in.readString();
    }

    public static final Parcelable.Creator<Repo> CREATOR = new Parcelable.Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel source) {
            return new Repo(source);
        }

        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };
}
