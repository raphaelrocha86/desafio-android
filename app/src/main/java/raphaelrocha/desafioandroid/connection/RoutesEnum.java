package raphaelrocha.desafioandroid.connection;

public enum RoutesEnum {

    REPOS_LIST(1),
    PULLS_REPO_LIST(2),
    USER_DATA(3);

    private final int value;
    RoutesEnum(int optionValue){
        value = optionValue;
    }

    public int getValue(){
        return value;
    }

}
