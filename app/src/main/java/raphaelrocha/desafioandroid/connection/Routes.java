package raphaelrocha.desafioandroid.connection;

public class Routes {

    private static final String SERVER = "https://api.github.com";
    public static final String REPO_LIST = SERVER+"/search/repositories?q=language:Java&sort=stars&page=:page";
    public static final String PULLS_REPO_LIST = SERVER+"/repos/:owner_login/:repo_name/pulls";
    public static final String USER_DATA = SERVER+"/user";

}