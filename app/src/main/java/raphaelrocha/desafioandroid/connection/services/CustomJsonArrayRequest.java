package raphaelrocha.desafioandroid.connection.services;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class CustomJsonArrayRequest extends Request<JSONArray> {
	private Listener<JSONArray> response;
	private Map<String, String> params;
	private Map<String, String> header;

    private String TAG = this.getClass().getSimpleName();

	public CustomJsonArrayRequest(int method, String url, Map<String, String> params, Listener<JSONArray> response, ErrorListener listener, Map<String, String> header) {
		super(method, url, listener);
		this.params = params;
		this.response = response;
		this.header = header;
	}

	public CustomJsonArrayRequest(String url, Map<String, String> params, Listener<JSONArray> response, ErrorListener listener) {
		super(Method.GET, url, listener);
		this.params = params;
		this.response = response;
	}
	
	public Map<String, String> getParams() throws AuthFailureError {
		return params;
	}
	
	public Map<String, String> getHeaders() throws AuthFailureError {
		return header;
	}
	
	public Priority getPriority(){
		return(Priority.NORMAL);
	}
	
	@Override
	protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
		try {
			String js = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//			Log.i(TAG, js);
			JSONArray ja = new JSONArray(js);
			Cache.Entry parser = HttpHeaderParser.parseCacheHeaders(response);
//			Log.w(TAG, ja.toString());
			return(Response.success(ja,parser));
		}
		catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
			Log.e(TAG,response.toString());
		}
		return null;
	}

//	public Listener getJa(){
//		return this.response;
//	}
	
	@Override
	protected void deliverResponse(JSONArray response) {
		this.response.onResponse(response);
	}
}