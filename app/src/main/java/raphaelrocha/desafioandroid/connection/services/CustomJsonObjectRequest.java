package raphaelrocha.desafioandroid.connection.services;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class CustomJsonObjectRequest extends Request<JSONObject> {
	private Listener<JSONObject> response;
	private Map<String, String> params;
	private Map<String, String> header;
	private String TAG = this.getClass().getSimpleName();

	public CustomJsonObjectRequest(int method, String url, Map<String, String> params, Listener<JSONObject> response, ErrorListener listener, Map<String, String> header) {
		super(method, url, listener);
		this.params = params;
		this.response = response;
		this.header = header;
	}

	public CustomJsonObjectRequest(String url, Map<String, String> params, Listener<JSONObject> response, ErrorListener listener) {
		super(Method.GET, url, listener);
		this.params = params;
		this.response = response;
	}
	
	public Map<String, String> getParams() throws AuthFailureError {
		return params;
	}
	
	public Map<String, String> getHeaders() throws AuthFailureError {
		return header;
	}

	@Override
	public byte[] getBody() throws AuthFailureError {
		Gson gson = new Gson();
		String json = gson.toJson(params);
		return json.getBytes();
	}
	
	public Priority getPriority(){
		return(Priority.NORMAL);
	}

	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		try {
			String js = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//			Log.i(TAG, js);
			JSONObject jo = new JSONObject(js);
			Cache.Entry parser = HttpHeaderParser.parseCacheHeaders(response);
//			Log.w(TAG, jo.toString());
			return(Response.success(jo,parser));
		}
		catch (UnsupportedEncodingException | JSONException e) {
			e.printStackTrace();
			Log.e(TAG,response.toString());
		}
		return null;
	}

	@Override
	protected void deliverResponse(JSONObject response) {
		this.response.onResponse(response);
	}

}
