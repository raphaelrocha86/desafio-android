package raphaelrocha.desafioandroid.connection.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

import raphaelrocha.desafioandroid.connection.RoutesEnum;
import raphaelrocha.desafioandroid.connection.ErrorMessage;

public interface CustomVolleyCallbackInterface {

    /*NAO ESQUECA DE IMPLEMENTAR O METODO OnStop() COM A CHAMADA PARA MreÉTODO cancelRequest NA CLASSE
    *VolleyConnectionQueue,
    * Exemplo:
         @Override
         public void onStop(){
            super.onStop();
            mVolleyConnection.cancelRequest();
         }
    * ONDE mVolleyConnection É UMA INSTÂNCIA DA CLASSE VolleyConnection
    */

    //TRATA RESPOSTAS DE SUCESSO
    public void deliveryResponse(JSONArray response, RoutesEnum routeEnum) throws Exception;
    public void deliveryResponse(JSONObject response, RoutesEnum routeEnum)  throws Exception;

    //TRATA RESPOSTAS DE ERRO
    public void deliveryError(ErrorMessage errorMessage, RoutesEnum routeEnum)  throws Exception;


}
