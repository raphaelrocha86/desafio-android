package raphaelrocha.desafioandroid.connection;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import raphaelrocha.desafioandroid.R;
import raphaelrocha.desafioandroid.connection.interfaces.CustomVolleyCallbackInterface;
import raphaelrocha.desafioandroid.connection.services.CustomJsonArrayRequest;
import raphaelrocha.desafioandroid.connection.services.CustomJsonObjectRequest;
import raphaelrocha.desafioandroid.connection.services.VolleyConnectionQueue;
import raphaelrocha.desafioandroid.utils.PrefManager;

public class VolleyConnection {

    private CustomVolleyCallbackInterface mCustomVolleyCallbackInterface;
    private Context context;
    private String mVOLLEYTAG;
    private final static String TAG = VolleyConnection.class.getSimpleName();

    public VolleyConnection(CustomVolleyCallbackInterface cvci, Context context){
        VolleyConnectionQueue.getINSTANCE(); //inicia a fila de requisições
        this.mCustomVolleyCallbackInterface = cvci;
        this.context = context;
    }

    private void setVolleyTag(String tag){
        Log.i(TAG, "setVolleyTag(" + tag + ")");
        this.mVOLLEYTAG = tag;
    }

    public void cancelRequest(){
        if(mVOLLEYTAG!=null){
            VolleyConnectionQueue.getINSTANCE().cancelRequest(this.mVOLLEYTAG);
        }
    }

    public void callServerApiByJsonArrayRequest(final String url, int requestMethod, boolean retyrPolicyForUpload, HashMap<String, String> params, final RoutesEnum routeEnum){

        final String activityName = mCustomVolleyCallbackInterface.getClass().getSimpleName();
        Log.i(TAG,"SEND MESSAGE URL["+activityName+"] : "+url);

        switch (requestMethod){
            case 0:
                Log.i(TAG,"METHOD GET ["+activityName+"] : ");
                break;
            case 1:
                Log.i(TAG,"METHOD POST ["+activityName+"] : ");
                break;
            case 2:
                Log.i(TAG,"METHOD PUT ["+activityName+"] : ");
                break;
            case 3:
                Log.i(TAG,"METHOD DELETE ["+activityName+"] : ");
                break;
        }

        Log.i(TAG,"SEND MESSAGE FLAG ["+activityName+"] : "+routeEnum);

        if(params!=null){
           Log.i(TAG, "SEND MESSAGE PARAMS [" + activityName + "] : " + params.toString());
        }

        //Header
        HashMap<String, String> header = new HashMap<>();
        String auth = PrefManager.getToken(context);
        if(auth!=null){
            header.put("Authorization", auth);
//            Log.i(TAG, "SEND MESSAGE HEADERS [" + activityName + "] : " + header.toString());
            Log.i(TAG, "SEND MESSAGE HEADERS [" + activityName + "] : true");
        }

        //Request.Method.POST
        CustomJsonArrayRequest request = new CustomJsonArrayRequest(requestMethod,
                url,
                params,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.w(TAG,"Delivery sucess to ["+activityName+"] url: ["+url+"] flag: ["+routeEnum+"]");
                            mCustomVolleyCallbackInterface.deliveryResponse(response,routeEnum);
                        } catch (Exception e) {
                            Log.e(TAG,"Delivery result in error ["+activityName+"] url: ["+url+"] flag: ["+routeEnum+"]");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "url: "+url+" | ERROR: " + error+" | flag: "+routeEnum);
                        ErrorMessage errorMessage = null;
                        try{
                            errorMessage = VolleyConnection.parseNetworkError(error,context);
                            if(errorMessage!=null){
                                Log.e(TAG,errorMessage.getMessage());
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

//                        if(error!=null && error.networkResponse!=null){
//                            if(error.networkResponse.statusCode == 401){
//                            }
//
//                            if(error.networkResponse.statusCode == 403){
//                            }
//                        }

                        try {
                            Log.e(TAG,"Delivery Error to ["+activityName+"] url: ["+url+"] flag: ["+routeEnum+"]");
                            mCustomVolleyCallbackInterface.deliveryError(errorMessage, routeEnum);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },header);

        if(retyrPolicyForUpload){
            request.setRetryPolicy(
                    new DefaultRetryPolicy(
                            500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    )
            );
        }else{
            request.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS+27500,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    )
            );
        }
        this.setVolleyTag(activityName);
        request.setTag(activityName);
        request.setShouldCache(true);
        VolleyConnectionQueue.getINSTANCE().addQueue(request);
    }

    //METODO PARA ENVIO E RECEBIMENTO DE JSONOBJECTS
    public void callServerApiByJsonObjectRequest(final String url, int requestMethod, boolean retyrPolicyForUpload, HashMap<String, String> params, final RoutesEnum routeEnum){

        final String activityName = mCustomVolleyCallbackInterface.getClass().getSimpleName();
        Log.i(TAG,"SEND MESSAGE URL ["+activityName+"] : "+url);

        switch (requestMethod){
            case 0:
               Log.i(TAG,"METHOD GET ["+activityName+"] : ");
                break;
            case 1:
               Log.i(TAG,"METHOD POST ["+activityName+"] : ");
                break;
            case 2:
               Log.i(TAG,"METHOD PUT ["+activityName+"] : ");
                break;
            case 3:
               Log.i(TAG,"METHOD DELETE ["+activityName+"] : ");
                break;
        }

        Log.i(TAG,"SEND MESSAGE FLAG ["+activityName+"] : "+routeEnum);

        if(params!=null){
            Log.i(TAG, "SEND MESSAGE PARAMS [" + activityName + "] : " + params.toString());
        }

        //Header
        HashMap<String, String> header = new HashMap<>();
        String auth = PrefManager.getToken(context);
        if(auth!=null){
            header.put("Authorization", auth);
//            Log.i(TAG, "SEND MESSAGE HEADERS [" + activityName + "] : " + header.toString());
            Log.i(TAG, "SEND MESSAGE HEADERS [" + activityName + "] : true");
        }

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(requestMethod,
                url,
                params,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        //envia a resposta de sucesso para a activity
                        try {
                            Log.w(TAG,"Delivery sucess to ["+activityName+"] url: ["+url+"] flag: ["+routeEnum+"]");
                            mCustomVolleyCallbackInterface.deliveryResponse(response, routeEnum);
                        } catch (Exception e) {
                            Log.e(TAG,"Delivery result in error ["+activityName+"] url: ["+url+"] flag: ["+routeEnum+"]");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "url: "+url+" | ERROR: " + error+" | flag: "+routeEnum);
                        ErrorMessage errorMessage = null;
                        try{
                            errorMessage = VolleyConnection.parseNetworkError(error,context);
                            if(errorMessage!=null){
                                Log.e(TAG,errorMessage.getMessage());
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

//                        if(error!=null && error.networkResponse!=null){
//                            if(error.networkResponse.statusCode == 401){
//                            }
//
//                            if(error.networkResponse.statusCode == 403){
//                            }
//                        }

                        try {
                            Log.e(TAG,"Delivery Error to ["+activityName+"] url: ["+url+"] flag: ["+routeEnum+"]");
                            mCustomVolleyCallbackInterface.deliveryError(errorMessage, routeEnum);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },header);

        if(retyrPolicyForUpload){
            request.setRetryPolicy(
                    new DefaultRetryPolicy(
                            500000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    )
            );
        }else{
            request.setRetryPolicy(
                    new DefaultRetryPolicy(
                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS+27500,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    )
            );
        }
        this.setVolleyTag(activityName);
        request.setTag(activityName);
        request.setShouldCache(true);
        VolleyConnectionQueue.getINSTANCE().addQueue(request);
    }

    //parser da resposta de erro
    private static ErrorMessage parseNetworkError(VolleyError volleyError, Context context){
        ErrorMessage errorMessage = new ErrorMessage();
        try {

            if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                Log.e(TAG,"error code: "+volleyError.networkResponse.statusCode);
                VolleyError v = new VolleyError(new String(volleyError.networkResponse.data));
                Gson gson = new Gson();
                errorMessage = gson.fromJson(v.getMessage(),ErrorMessage.class);
                errorMessage.setCode(volleyError.networkResponse.statusCode);
            }else{
                /*Bad gateway*/
                errorMessage.setCode(502);
                errorMessage.setMessage(context.getResources().getString(R.string.error_bad_gateway));
            }

        }catch (Exception e) {
            e.printStackTrace();
            errorMessage.setCode(-1);
            errorMessage.setMessage(context.getResources().getString(R.string.error_undefined));
        }
        return errorMessage;
    }
}
