package raphaelrocha.desafioandroid.connection;

import com.google.gson.annotations.SerializedName;

public class ErrorMessage {
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "message='" + message + '\'' +
                ", code=" + code +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
