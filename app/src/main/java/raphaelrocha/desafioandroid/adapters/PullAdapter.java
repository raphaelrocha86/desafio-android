package raphaelrocha.desafioandroid.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import raphaelrocha.desafioandroid.R;
import raphaelrocha.desafioandroid.interfaces.RecyclerViewOnClickListener;
import raphaelrocha.desafioandroid.models.Pull;

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.MyViewHolder> {

//    private final String TAG = this.getClass().getSimpleName();
    private List<Pull> mList;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListener;
    private Context mContext;

    public PullAdapter(List<Pull> list, Context c) {
        this.mList = list;
        this.mContext = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_pull, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {

            final Pull pull = mList.get(position);

            //load de imagem com cache
            Picasso.with(mContext)
                .load(pull.getUser().getAvatarUrl())
                .placeholder(ContextCompat.getDrawable(mContext,R.drawable.avatar_default))
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(mContext)
                            .load(pull.getUser().getAvatarUrl())
                            .error(ContextCompat.getDrawable(mContext,R.drawable.avatar_default))
                            .placeholder(ContextCompat.getDrawable(mContext,R.drawable.avatar_default))
                            .into(holder.avatar, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    Log.v("Picasso","Could not fetch image");
                                }
                            });
                    }
                });

            holder.pullTitle.setText(pull.getTitle());
            holder.username.setText(pull.getUser().getLogin());
            holder.idUSer.setText(String.valueOf(pull.getUser().getId()));
            holder.pullDescription.setText(pull.getBody());

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SSS", Locale.getDefault());
            sdf1.setTimeZone(TimeZone.getTimeZone(Calendar.getInstance().getTimeZone().toString()));
            Date theDate = sdf1.parse(pull.getCreatedAt());
            Timestamp ts = new Timestamp(theDate.getTime());
            String timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(ts);

            holder.pushedAt.setText(String.format(mContext.getString(R.string.pull_timestamp),timestamp));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Pull> l){
        this.mList = l;
        notifyDataSetChanged();
    }

    public void setRecyclerViewOnClickListener(RecyclerViewOnClickListener r){
        mRecyclerViewOnClickListener = r;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private CircleImageView avatar;
        private TextView pullTitle;
        private TextView username;
        private TextView idUSer;
        private TextView pullDescription;
        private TextView pushedAt;

        MyViewHolder(View view) {
            super(view);

            avatar = (CircleImageView) itemView.findViewById(R.id.user_avatar);
            pullTitle = (TextView) itemView.findViewById(R.id.pull_title);
            username = (TextView) itemView.findViewById(R.id.username);
            idUSer = (TextView) itemView.findViewById(R.id.id_user);
            pullDescription = (TextView) itemView.findViewById(R.id.pull_description);
            pushedAt = (TextView) itemView.findViewById(R.id.pull_pushed_at);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListener != null){
                mRecyclerViewOnClickListener.onClickListener(v, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if(mRecyclerViewOnClickListener != null){
                mRecyclerViewOnClickListener.onLongPressClickListener(v, getAdapterPosition());
                return true;
            }
            return false;
        }
    }


}
