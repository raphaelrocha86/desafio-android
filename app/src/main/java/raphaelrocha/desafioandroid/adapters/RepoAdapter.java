package raphaelrocha.desafioandroid.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import raphaelrocha.desafioandroid.R;
import raphaelrocha.desafioandroid.activities.MainActivity;
import raphaelrocha.desafioandroid.app.Config;
import raphaelrocha.desafioandroid.interfaces.RecyclerViewOnClickListener;
import raphaelrocha.desafioandroid.models.Repo;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.MyViewHolder> {

//    private String TAG = this.getClass().getSimpleName();
    private List<Repo> mList;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListener;
    private int mPageToLoadCount;
    private int mCurrentElement;
    private Context mContext;

    public RepoAdapter(List<Repo> list,  Context c) {
        this.mList = list;
        this.mContext = c;
        /*
        este contador de páginas inicia em 1 pois observei que as páginas 0 e página 1 possuem o
        resultad
        */
        this.mPageToLoadCount = Config.DEFAULT_PAGE_REPO_LIST;
        this.mCurrentElement = 0;
    }

    public int getPageToLoadCount(){
        return this.mPageToLoadCount;
    }

    public void setPageToLoadCount(int countPageToLoad){
        this.mPageToLoadCount = countPageToLoad;
    }

    public boolean isTimeToLoad(){
        return mCurrentElement >= mList.size() - 10;
    }

    public void resetPageToLoad(){
        this.mPageToLoadCount = Config.DEFAULT_PAGE_REPO_LIST;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_repo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {

            final Repo repo = mList.get(position);

            mCurrentElement = position+1;

            if(mCurrentElement == mList.size()-10){
//                Log.w(TAG,"current : "+mCurrentElement+" | size: "+mList.size());
                ((MainActivity)mContext).getListRepoFromServer(mPageToLoadCount);
            }

            if(mCurrentElement == mList.size()){
//                Log.w(TAG,"current : "+mCurrentElement+" | size: "+mList.size());
                ((MainActivity)mContext).forceShowProgress();
            }

            //load de imagem com cache
            Picasso.with(mContext)
                .load(repo.getOwner().getAvatarUrl())
                .placeholder(ContextCompat.getDrawable(mContext,R.drawable.avatar_default))
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(mContext)
                            .load(repo.getOwner().getAvatarUrl())
                            .error(ContextCompat.getDrawable(mContext,R.drawable.avatar_default))
                            .placeholder(ContextCompat.getDrawable(mContext,R.drawable.avatar_default))
                            .into(holder.avatar, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
//                                    Log.v("Picasso","Could not fetch image");
                                }
                            });
                    }
                });

            holder.repoName.setText(repo.getName());
            holder.usename.setText(repo.getOwner().getLogin());
            holder.repoDescription.setText(repo.getDescription());

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SSS", Locale.getDefault());
            sdf1.setTimeZone(TimeZone.getTimeZone(Calendar.getInstance().getTimeZone().toString()));
            Date theDate = sdf1.parse(repo.getPushedAt());
            Timestamp ts = new Timestamp(theDate.getTime());
            String timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(ts);

            holder.pushedAt.setText(String.format(mContext.getString(R.string.repo_timestamp),timestamp));
            holder.countStars.setText(String.valueOf(repo.getStars()));
            holder.countForks.setText(String.valueOf(repo.getForks()));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Repo> l){
        this.mList = l;
        this.mPageToLoadCount++;
        notifyDataSetChanged();
    }

    public void setRecyclerViewOnClickListener(RecyclerViewOnClickListener r){
        mRecyclerViewOnClickListener = r;
    }

    public Repo getItem(int position){
        return mList.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private CircleImageView avatar;
        private TextView repoName;
        private TextView usename;
        private TextView repoDescription;
        private TextView pushedAt;
        private TextView countStars;
        private TextView countForks;

        MyViewHolder(View view) {
            super(view);

            avatar = (CircleImageView) itemView.findViewById(R.id.user_avatar);
            repoName = (TextView) itemView.findViewById(R.id.repo_name);
            usename = (TextView) itemView.findViewById(R.id.username);
            repoDescription = (TextView) itemView.findViewById(R.id.repo_description);
            pushedAt = (TextView) itemView.findViewById(R.id.repo_pushed_at);
            countStars = (TextView) itemView.findViewById(R.id.repo_count_stars);
            countForks = (TextView) itemView.findViewById(R.id.repo_count_forks);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListener != null){
                mRecyclerViewOnClickListener.onClickListener(v, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if(mRecyclerViewOnClickListener != null){
                mRecyclerViewOnClickListener.onLongPressClickListener(v, getAdapterPosition());
                return true;
            }
            return false;
        }
    }
}
